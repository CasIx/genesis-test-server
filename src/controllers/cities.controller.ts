import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { City } from "../entities";
import { CitiesService } from "../providers";

@Crud({
    model: {
        type: City
    },
    query: {
        alwaysPaginate: true,
    },
    routes: {
        only: ['getManyBase', 'getOneBase']
    }
})
@ApiTags('Cities')
@Controller('/cities')
export class CitiesController implements CrudController<City> {
    constructor(public readonly service: CitiesService) {  }
}