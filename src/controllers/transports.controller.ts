import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { Crud, CrudController, CrudService } from "@nestjsx/crud";
import { Transport } from "../entities";
import { TransportsService } from "../providers";

@Crud({
    model: {
        type: Transport
    },
    query: {
        alwaysPaginate: true
    },
    routes: {
        exclude: ['createManyBase', 'replaceOneBase', 'recoverOneBase']
    },
})
@ApiTags('Transports')
@Controller('/transports')
export class TransportsController implements CrudController<Transport> {
    constructor(public readonly service: TransportsService) { }
}