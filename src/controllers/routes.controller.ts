import { Controller } from "@nestjs/common";
import { ApiTags } from "@nestjs/swagger";
import { Crud, CrudController } from "@nestjsx/crud";
import { Route } from "../entities";
import { RoutesService } from "../providers";

@Crud({
    model: {
        type: Route
    },
    query: {
        alwaysPaginate: true,
        join: {
            'startCity': {},
            'transport': {},
            'finishCity': {},
        }
    },
    routes: {
        exclude: ['createManyBase', 'replaceOneBase', 'recoverOneBase']
    }
})
@ApiTags('Routes')
@Controller('/routes')
export class RoutesController implements CrudController<Route> {
    constructor(public readonly service: RoutesService) { }
}