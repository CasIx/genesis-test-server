import { CitiesController } from "./cities.controller";
import { RoutesController } from "./routes.controller";
import { TransportsController } from "./transports.controller";

export default [
    CitiesController,
    RoutesController,
    TransportsController,
]

export { 
    CitiesController,
    RoutesController,
    TransportsController,
}