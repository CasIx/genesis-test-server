import { CitiesService } from "./cities.service"
import { RoutesService } from "./routes.service"
import { TransportsService } from "./transports.service"

export default [
    TransportsService,
    RoutesService,
    CitiesService,
]

export {
    TransportsService,
    RoutesService,
    CitiesService,
}