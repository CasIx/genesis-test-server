import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { City } from "../entities";

@Injectable()
export class CitiesService extends TypeOrmCrudService<City> {
    constructor(@InjectRepository(City) repo: Repository<City>) { super(repo) }
}