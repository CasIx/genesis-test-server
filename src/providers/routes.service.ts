import { RouteStatus, TransportStatus } from "@genesis-test/core";
import { BadRequestException, Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { CrudRequest } from "@nestjsx/crud";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { DeepPartial, getManager, getRepository, Repository } from "typeorm";
import { City, Route, Transport } from "../entities";

@Injectable()
export class RoutesService extends TypeOrmCrudService<Route> {
    constructor(@InjectRepository(Route) repo: Repository<Route>) { super(repo) }
    async createOne(req: CrudRequest, dto: DeepPartial<Route>) {
        const transport = await getRepository(Transport).findOne(dto.transport?.id ?? dto.transportId);
        const startCity = await getRepository(City).findOne(dto.startCity.id ?? dto.startCityId);
        const finishCity = await getRepository(City).findOne(dto.finishCity.id ?? dto.finishCityId);

        if (transport.type != dto.transportType) {
            throw new BadRequestException('transportType must be the same as transport.type');
        }

        return super.createOne(req, {
            ...dto,
            distance: this.getDistance(startCity, finishCity),
            startDate: null,
            finishDate: null,
            status: RouteStatus.Pending,
        });
    }

    async updateOne(req: CrudRequest, dto: DeepPartial<Route>) {
        const route = await this.getOne(req);
        route.transport = await getRepository(Transport).findOne(route.transportId);
        if (route.status == RouteStatus.Finished) {
            throw new BadRequestException("You can't update finished route");
        }
        if(route.transport.id != (dto.transport?.id ?? dto.transportId)) {
            if(route.status != RouteStatus.Pending) { 
                throw new BadRequestException("You can't change transport after starting");
            }
        }
        if (route.status == RouteStatus.Pending) {
            if (dto.status == RouteStatus.InProgress) {
                if (route.transport.status == TransportStatus.Busy) {
                    throw new BadRequestException("Transport for this route is busy. Change transport or wait")
                }
                dto.transport = {
                    ...route.transport,
                    status: TransportStatus.Busy
                }
                dto.startDate = new Date()
            }
        }
        if (route.status == RouteStatus.InProgress) {
            if (dto.status == RouteStatus.Pending) {
                throw new BadRequestException("You can't rollback route status to pending");
            }
            if (dto.status == RouteStatus.Finished) {
                dto.finishDate = new Date();
                dto.transport = {
                    ...route.transport,
                    status: TransportStatus.Free,
                    mileage: route.transport.mileage + route.distance,
                }
            }
        }

        await getManager()
            .transaction(async manager => {
                await manager.save(this.repo.create({ id: route.id, ...dto }));
                await manager.save(getRepository(Transport).create(dto.transport));
                await manager.save(getRepository(City).create(dto.startCity));
                await manager.save(getRepository(City).create(dto.finishCity));
            })
            return this.getOne(req);
    }


    getDistance(startCity: City, finishCity: City) {
        const R = 6371e3;
        const f1 = startCity.lat * Math.PI / 180;
        const f2 = finishCity.lat * Math.PI / 180;
        const d1 = (finishCity.lat - startCity.lat) * Math.PI / 180;
        const d2 = (finishCity.lng - startCity.lng) * Math.PI / 180;

        const a = Math.sin(d1 / 2) * Math.sin(d1 / 2) +
            Math.cos(f1) * Math.cos(f2) *
            Math.sin(d2 / 2) * Math.sin(d2 / 2);
        const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        const d = R * c;
        return Math.round((d * 0.001 + Number.EPSILON) * 100) / 100
    }
}