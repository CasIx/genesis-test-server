import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { TypeOrmCrudService } from "@nestjsx/crud-typeorm";
import { Repository } from "typeorm";
import { Transport } from "../entities";

@Injectable()
export class TransportsService extends TypeOrmCrudService<Transport> {
    constructor(@InjectRepository(Transport) repo: Repository<Transport>) { super(repo) }
}