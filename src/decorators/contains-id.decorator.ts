import { PrimaryGeneratedColumn } from "typeorm";
import { decorator } from "./decorator";
import { ApiProperty } from "@nestjs/swagger";

export const ContainsId = decorator('id', ApiProperty({ type: Number }), PrimaryGeneratedColumn());
export interface IContainsId {
    id: number;
}