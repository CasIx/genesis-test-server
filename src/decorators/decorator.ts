export function decorator(field: string, ...decorators: Function[]) {
    return <T extends { new(...args: any[]): {} }>(constructor: T) => {
        const r = Reflect.decorate(decorators as any, constructor.prototype, field, { writable: true })
        Object.defineProperty(constructor, field, r);
        
        return constructor;
    }
}