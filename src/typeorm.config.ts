import { join } from 'path'
import { SnakeNamingStrategy } from "typeorm-naming-strategies";
import dotenv from 'dotenv'

dotenv.config()

export default {
    host: process.env.DB_HOST ?? 'localhost',
    port: Number.parseInt(process.env.DB_PORT ?? "3306"),
    type: process.env.DB_TYPE ?? 'mysql' as any,
    username: process.env.DB_USER ?? 'root',
    password: process.env.DB_PASSWORD ?? 'rootroot',
    database: process.env.DB_NAME ?? 'genesis-test',
    entities: [join(__dirname, "./entities/*.entity{.ts,.js}")],
    migrationsTableName: "orm_migrations",
    migrations: [join(__dirname, "./migrations/*{.ts,.js}")],
    namingStrategy: new SnakeNamingStrategy(),
    synchronize: true,
    watch: true,

}