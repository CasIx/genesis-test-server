import { Module } from "@nestjs/common";
import providers from './providers'
import controllers from './controllers'
import entities from './entities'
import ormconfig from './typeorm.config';
import { TypeOrmModule } from "@nestjs/typeorm";

@Module({
    imports: [
        TypeOrmModule.forRoot(ormconfig),
        TypeOrmModule.forFeature(entities),
    ],
    providers,
    controllers,
})
export class AppModule {

}