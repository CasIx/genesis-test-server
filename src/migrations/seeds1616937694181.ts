import { getManager, getRepository, MigrationInterface, QueryRunner } from "typeorm";
import { readFileSync } from 'fs';
import { City, Transport } from "../entities";
import { TransportStatus, TransportType } from "@genesis-test/core";
import { join } from "path";
export class Seeds1616937694181 implements MigrationInterface {
    name?: string;
    async up(queryRunner: QueryRunner): Promise<any> {
        //https://gist.github.com/alex-oleshkevich/1509c308fabab9e104b5190dab99a77b
        const cities = JSON.parse(readFileSync(join(__dirname, 'cities.json')).toString());
        await getRepository(City).save(cities);
        await getRepository(Transport).save([{
            type: TransportType.Freight,
            model: 'MAN',
            number: 'AT0000AT',
            mileage: 1000,
        }, {
            type: TransportType.Cars,
            model: 'Lada',
            number: 'AT0001AT',
            mileage: 1000000000,
        },  {
            type: TransportType.Carriage,
            model: 'ECO',
            number: 'AT1209341209AT',
            mileage: 10
        }])

    }
    down(queryRunner: QueryRunner): Promise<any> {
        throw new Error("Method not implemented.");
    }

}