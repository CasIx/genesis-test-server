import { ITransport, TransportStatus, TransportType } from "@genesis-test/core";
import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity } from "typeorm";
import { ContainsId, IContainsId } from "../decorators";

@Entity('transports')
@ContainsId
export class Transport implements ITransport {

    @ApiProperty()
    @Column()
    number: string;

    @ApiProperty()
    @Column()
    model: string;

    @ApiProperty({
        enum: TransportType,
        enumName: 'TransportType'
    })
    @Column({ enum: TransportType, type: 'enum' })
    type: TransportType;

    @ApiProperty()
    @Column({ type: 'double' })
    mileage: number;

    @ApiProperty({
        enum: TransportStatus,
        enumName: 'TransportStatus',
    })
    @Column({ enum: TransportStatus, type: 'enum', default: TransportStatus.Free })
    status: TransportStatus;

}

export interface Transport extends IContainsId { 

}