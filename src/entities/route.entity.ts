import { Column, Entity, JoinColumn, ManyToOne } from "typeorm";
import { Currency, ICity, IRoute, ITransport, RouteStatus, TransportType } from "@genesis-test/core"
import { ContainsId, IContainsId } from "../decorators";
import { City } from "./city.entity";
import { ApiProperty } from "@nestjs/swagger";
import { Transport } from ".";

@Entity('routes')
@ContainsId
export class Route implements IRoute {
    
    @ApiProperty({
        type: () => Transport
    })
    @ManyToOne(() => Transport, { nullable: true, cascade: ['update']})
    transport: ITransport;

    @ApiProperty()
    @Column({ nullable: true })
    transportId: number;

    @ApiProperty({
        enum: RouteStatus,
        enumName: 'RouteStatus'
    })
    @Column({ enum: RouteStatus, type: 'enum'})
    status: RouteStatus;

    @ApiProperty()
    @Column({ nullable: true })
    startDate: Date;

    @ApiProperty()
    @Column({ nullable: true })
    finishDate: Date;

    @ApiProperty({
        type: () => City
    })
    @ManyToOne(() => City)
    @JoinColumn()
    startCity: ICity;

    @ApiProperty()
    @Column()
    startCityId: number;

    @ApiProperty({
        type: () => City
    })
    @ManyToOne(() => City)
    @JoinColumn()
    finishCity: ICity;

    @ApiProperty()
    @Column()
    finishCityId: number;

    @ApiProperty()
    @Column({ type: "real" })
    distance: number;

    @ApiProperty({
        enum: TransportType,
        enumName: 'TransportType'
    })
    @Column({ enum: TransportType, type: 'enum' })
    transportType: TransportType;

    @ApiProperty({ type: 'real' })
    @Column()
    estimatedRevenue: number;

    @ApiProperty({
        enum: Currency,
        enumName: 'Currency',
    })
    @ApiProperty({ enum: Currency, type: 'enum' })
    @Column({ enum: Currency, type: 'enum', nullable: true })
    revenueCurrency: Currency;

}

export interface Route extends IContainsId {

}