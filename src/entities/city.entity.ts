import { ICity } from "@genesis-test/core";
import { ApiProperty } from "@nestjs/swagger";
import { Column, Entity } from "typeorm";
import { ContainsId, IContainsId } from "../decorators";


@Entity('cities')
@ContainsId
export class City implements ICity {

    @ApiProperty()
    @Column()
    name: string;

    @ApiProperty()
    @Column({ type: 'double' })
    lat: number;

    @ApiProperty()
    @Column({ type: 'double' })
    lng: number;

}

export interface City extends IContainsId { 

}