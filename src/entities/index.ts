import { City } from "./city.entity"
import { Route } from "./route.entity"
import { Transport } from "./transport.entity"

export {
    City,
    Route,
    Transport,
}

export default [
    City,
    Route,
    Transport,
]