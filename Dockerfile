FROM node:12.16.3-alpine AS runner
WORKDIR /app
COPY ./dist /app
RUN ls -la
CMD ["sh", "-c", "node main.js"]
